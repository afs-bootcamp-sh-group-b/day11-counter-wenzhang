/* eslint-disable no-case-declarations */
import { SET_COUNTER_SIZE, UPDATE_SUM } from "./actions";

const initialState = {
  counterSize: 0,
  sum: 0,
};

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_COUNTER_SIZE:
      return {
        ...state,
        counterSize: action.payload,
      };
    case UPDATE_SUM:
      return { ...state, sum: action.payload };
    default:
      return state;
  }
};

export default counterReducer;
