export const SET_COUNTER_SIZE = "SET_COUNTER_SIZE";
export const UPDATE_SUM = "UPDATE_SUM";
export const UPDATE_COUNTS = "UPDATE_COUNTS";

export const setCounterSize = (counterSize) => ({
  type: SET_COUNTER_SIZE,
  payload: counterSize,
});

export const updateSum = (sum) => ({
  type: UPDATE_SUM,
  payload: sum,
});
