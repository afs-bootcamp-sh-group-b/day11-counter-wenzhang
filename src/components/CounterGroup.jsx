import Counter from "./Counter";
import { useSelector } from "react-redux";

function CounterGroup() {
  const counterSize = useSelector((state) => state.counterSize);

  return (
    <>
      {Array.from({ length: counterSize }).map((_, index) => (
        <Counter key={index} />
      ))}
    </>
  );
}

export default CounterGroup;
