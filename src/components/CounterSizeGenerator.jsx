import { useDispatch, useSelector } from "react-redux";
import { setCounterSize } from "../redux/actions";
import { updateSum } from "../redux/actions";

function CounterSizeGenerator() {
  const dispatch = useDispatch();
  const counterSize = useSelector((state) => state.counterSize);

  const handleInputChange = (e) => {
    const inputSize = parseInt(e.target.value);
    dispatch(setCounterSize(inputSize));
    dispatch(updateSum(0));
  };

  return (
    <div>
      <span>Size: </span>
      <input
        type="number"
        value={counterSize}
        onChange={handleInputChange}
        min="0"
      />
    </div>
  );
}

export default CounterSizeGenerator;
