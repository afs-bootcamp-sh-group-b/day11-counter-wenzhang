import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { updateSum } from "../redux/actions";

function Counter() {
  const [count, setCount] = useState(0);
  const sum = useSelector((state) => state.sum);
  //const count = useSelector((state) => state.count[index]);
  const dispatch = useDispatch();
  const counterSize = useSelector((state) => state.counterSize);

  useEffect(() => {
    setCount(0);
  }, [counterSize]);

  const increment = () => {
    setCount((count) => count + 1);
    dispatch(updateSum(sum + 1));
  };

  const decrement = () => {
    setCount((count) => count - 1);
    dispatch(updateSum(sum - 1));
  };

  return (
    <>
      <div>
        <button onClick={increment}>+</button>
        <span>
          <div>count is</div>
          <div>{count}</div>
        </span>
        <button onClick={decrement}>-</button>
      </div>
    </>
  );
}

export default Counter;
