import CounterGroup from "./CounterGroup";
import CounterGroupSum from "./CounterGroupSum";
import CounterSizeGenerator from "./CounterSizeGenerator";
import { useSelector } from "react-redux";

function MultipleCounter() {
  const sum = useSelector((state) => state.sum);

  return (
    <>
      <CounterSizeGenerator />
      <CounterGroupSum sum={sum} />
      <CounterGroup />
    </>
  );
}

export default MultipleCounter;
