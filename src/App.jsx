import "./App.css";
import MultipleCounter from "./components/MultipleCounter";
import { Provider } from "react-redux";
import store from "./redux/store";

function App() {
  return (
    <>
      <Provider store={store}>
        <MultipleCounter />
      </Provider>
    </>
  );
}

export default App;
