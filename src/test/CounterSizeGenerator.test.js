/* eslint-disable no-undef */
import { render, screen } from "@testing-library/react";
import CounterSizeGenerator from "../components/CounterSizeGenerator";
import "@testing-library/jest-dom/extend-expect";

describe("counter size generator test", () => {
    it("should show counter size", () => {
        render(<CounterSizeGenerator counterSize={1}/>);
        const input = screen.getByRole("spinbutton");
        expect(input).toBeInTheDocument();
    });
});
