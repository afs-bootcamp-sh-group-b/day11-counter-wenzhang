/* eslint-disable no-undef */
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Counter from "../components/Counter";

describe("counter test", () => {
  it("should show zero as init value", () => {
    render(<Counter/>);

    const zeros = screen.getAllByText(0);

    expect(zeros.length).toBe(1);
  });

  it("should increase when click add button", () => {
    render(<Counter/>);

    const addButton = screen.getByRole("button", { name: "+" });

    fireEvent.click(addButton);

    expect(screen.getByText("1")).toBeInTheDocument();
  });

  it("should decrease when click minus button", () => {
    render(<Counter/>);

    const minusButton = screen.getByRole("button", { name: "-" });

    fireEvent.click(minusButton);

    expect(screen.getByText("-1")).toBeInTheDocument();
  });
});
