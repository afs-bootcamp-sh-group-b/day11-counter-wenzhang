/* eslint-disable no-undef */
import { render, screen} from '@testing-library/react';
import CounterGroup from "../components/CounterGroup"

describe("CounterGroup", () => {
    it("should include multiple zeros", () => {
        render(<CounterGroup counterSize={2}></CounterGroup>);
        const zeros = screen.getAllByText("0");
        expect(zeros.length).toBe(2);
    })
})
